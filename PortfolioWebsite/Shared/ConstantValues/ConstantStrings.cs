﻿namespace PortfolioWebsite.Shared.ConstantValues
{
	public static class ConstantStrings
	{
		/// <summary>
		/// Строка для хранения формата даты
		/// </summary>
		public const string DisplayShowDateMonthAndYear = "MMMM yyyy";
	}
}
