﻿using Blazored.LocalStorage;

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.JSInterop;

using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Pages
{
	public partial class Index : ComponentBase
	{
		[Inject] protected IStringLocalizer<Resource>? Localizer { get; private set; }
		[Inject] protected ILocalStorageService? LocalStorage { get; private set; }
		[Inject] protected NavigationManager? NavigationManager { get; private set; }
		[Inject] protected IJSRuntime? JSRuntime { get; private set; }

		private int _numberPage;
		private string? _culture;
		private string? _language;

		private readonly string[] _arrayLink = ["https://t.me/c60a81cefc9bfea8e45857ddeb6c58d3", "https://www.linkedin.com/in/illya-rybak-24442923a", "mailto:karosveduni@gmail.com", "https://gitlab.com/karosveduni"];

		private void SelectedPage(int numberPage)
		{
			_numberPage = numberPage;
		}

		private async Task OnCultureChanged(string culture)
		{
			if (!_culture?.Equals(culture) ?? true)
			{
				_culture = culture;
				await LocalStorage!.SetItemAsStringAsync("culture", culture);
				NavigationManager!.NavigateTo(NavigationManager.Uri, forceLoad: true);
				SetLanguage();
			}
		}

		private void SetLanguage()
		{
			_language = _culture switch
			{
				"ru-RU" => "Русский",
				"uk-UA" => "Українська",
				_ => "English",
			};
		}


		protected async override Task OnInitializedAsync()
		{
			_numberPage = 1;
			_culture = await LocalStorage!.GetItemAsStringAsync("culture");
			SetLanguage();
		}

		private async Task ClickLink(int indexLink)
		{
			await JSRuntime!.InvokeVoidAsync("open", _arrayLink[indexLink], "_blank");
		}
	}
}
