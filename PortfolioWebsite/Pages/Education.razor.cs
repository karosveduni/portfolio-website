﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Pages
{
	public partial class Education : ComponentBase
	{
		[Inject] protected IStringLocalizer<Resource>? Localizer { get; private set; }
	}
}
