﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Pages
{
	public partial class Projects : ComponentBase
	{
		[Inject] protected IStringLocalizer<Resource>? Localizer { get; set; }
	}
}
