﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Pages
{
	public partial class WorkExperience : ComponentBase
	{
		[Inject] IStringLocalizer<Resource>? Localizer { get; set; }
	}
}
