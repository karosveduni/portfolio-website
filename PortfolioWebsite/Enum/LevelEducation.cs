﻿namespace PortfolioWebsite.Enum
{
	/// <summary>
	/// Уровень образования.
	/// </summary>
	public enum LevelEducation
	{
		/// <summary>
		/// Среднее образование.
		/// </summary>
		Secondary,

		/// <summary>
		/// Среднее специальное образование.
		/// </summary>
		SecondarySpecial,

		/// <summary>
		/// Незаконченное высшее образование.
		/// </summary>
		IncompleteHigher,

		/// <summary>
		/// Высшее образование.
		/// </summary>
		Higher
	}
}