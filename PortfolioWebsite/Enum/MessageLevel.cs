﻿namespace PortfolioWebsite.Enum
{
	/// <summary>
	/// Уровни сообщений.
	/// </summary>
	public enum MessageLevel
	{
		/// <summary>
		/// Информационное сообщение.
		/// </summary>
		Info,

		/// <summary>
		/// Сообщение об успешном выполнении.
		/// </summary>
		Success,

		/// <summary>
		/// Предупреждающее сообщение.
		/// </summary>
		Warning,

		/// <summary>
		/// Сообщение об ошибке.
		/// </summary>
		Error
	}
}