﻿using Microsoft.AspNetCore.Components;

namespace PortfolioWebsite.Data
{
	/// <summary>
	/// Класс в котором хранится информация о проектах
	/// </summary>
	public class Project
	{
		/// <summary>
		/// Название проекта
		/// </summary>
		public string? NameProject { get; set; }
		/// <summary>
		/// Ссылка на проект
		/// </summary>
		public Uri? ProjectLink { get; set; }
		/// <summary>
		/// Ссылка на фото проекта
		/// </summary>
		public Uri? PhotoLink { get; set; }
		/// <summary>
		/// Описание проекта
		/// </summary>
		public MarkupString? Description { get; set; }
	}
}
