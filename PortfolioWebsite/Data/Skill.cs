﻿namespace PortfolioWebsite.Data
{
	/// <summary>
	/// Класс для хранения информации о навыках
	/// </summary>
	public class Skill
	{
		/// <summary>
		/// Название навыка
		/// </summary>
		public string? Name { get; set; }
	}
}
