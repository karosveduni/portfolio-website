﻿namespace PortfolioWebsite.Data
{
	/// <summary>
	/// Класс про опыт работы
	/// </summary>
	public class WorkExperience
	{
		/// <summary>
		/// Название компании
		/// </summary>
		public string? NameCompany { get; set; }
		/// <summary>
		/// Посада в компании
		/// </summary>
		public string? Position { get; set; }
		/// <summary>
		/// Дата начала работы
		/// </summary>
		public DateTime Start { get; set; }
		/// <summary>
		/// Конец работы
		/// </summary>
		public DateTime End { get; set; }
		/// <summary>
		/// Описание работы
		/// </summary>
		public string? WorkDescription { get; set; }
		/// <summary>
		/// Ссылка на фото компании
		/// </summary>
		public Uri? LinkPhoto { get; set; }
		/// <summary>
		/// Ссылка на сайт компании
		/// </summary>
		public Uri? LinkSiteCompany { get; set; }
		/// <summary>
		/// Хранит информацию про проекты сделаные в компании
		/// </summary>
		public KeyValuePair<string, ProjectLink>[]? Projects { get; set; }
	}

	/// <summary>
	/// Класс для хранения информации о проектах
	/// </summary>
	/// <param name="LinkAndroid">Ссылка на Android проект</param>
	/// <param name="LinkiOS">Ссылка на iOS проект</param>
	/// <param name="LinkWindow">Ссылка на Window проект</param>
	public record ProjectLink(string LinkAndroid, string LinkiOS, string LinkWindow);
}
