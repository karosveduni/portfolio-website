﻿using PortfolioWebsite.Enum;

namespace PortfolioWebsite.Data
{
	/// <summary>
	/// Класс в котором хранится информация об образовании
	/// </summary>
	public class Education
	{
		/// <summary>
		/// Название университета
		/// </summary>
		public string? NameUniversity { get; set; }
		/// <summary>
		/// Название специальности
		/// </summary>
		public string? Speciality { get; set; }
		/// <summary>
		/// Уровень образования
		/// </summary>
		public LevelEducation LevelEducation { get; set; }
		/// <summary>
		/// Год начала обучения
		/// </summary>
		public short StartDate { get; set; }
		/// <summary>
		/// Конец обучения
		/// </summary>
		public short EndDate { get; set; }
		/// <summary>
		/// Ссылка на фото университета
		/// </summary>
		public Uri? LinkPhoto { get; set; }
		/// <summary>
		/// Ссылка на сайт университета
		/// </summary>
		public Uri? LinkSiteUniversity { get; set; }
	}
}
