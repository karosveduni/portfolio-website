﻿using PortfolioWebsite.Data;
using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Services
{
	/// <summary>
	/// Сервис для работы с данными о рабочем опыте.
	/// </summary>
	public class WorkExperienceService
	{
		/// <summary>
		/// Данные о первом рабочем опыте.
		/// </summary>
		public static WorkExperience FirstWorkExperience { get; } = new WorkExperience
		{
			NameCompany = "Microinvest",
			Position = "Xamarin developer",
			Start = new DateTime(2021, 7, 1),
			End = new DateTime(2022, 5, 1),
			WorkDescription = Resource.MicroinvestWorkDescription,
			LinkPhoto = new Uri("https://microinvest.ua/img/logo.png"),
			LinkSiteCompany = new Uri("https://microinvest.su/"),
			Projects =
			[
				KeyValuePair.Create("Sendera", new ProjectLink(LinkAndroid: "https://play.google.com/store/apps/details?id=net.Microinvest.Sendera&hl=ru&gl=US",
																LinkiOS: "https://apps.apple.com/ua/app/sendera/id1526642579?l=uk",
																LinkWindow: "")),
				KeyValuePair.Create("Microinvest Control Pro", new ProjectLink(LinkAndroid: "https://play.google.com/store/apps/details?id=net.controlpro&hl=ru&gl=US",
																				LinkiOS: "",
																				LinkWindow: "")),
				KeyValuePair.Create("Хотесса", new ProjectLink(LinkAndroid: "https://play.google.com/store/apps/details?id=microinvest.Hotessa&hl=ru&gl=US",
																LinkiOS: "",
																LinkWindow: ""))
			],
		};

		/// <summary>
		/// Данные о втором рабочем опыте.
		/// </summary>
		public static WorkExperience SecondWorkExperience { get; } = new WorkExperience
		{
			NameCompany = Resource.NameCLF,
			Position = "Xamarin developer",
			Start = new DateTime(2022, 5, 1),
			End = DateTime.MinValue,
			WorkDescription = Resource.UEFWorkDescription,
			LinkPhoto = new Uri("https://www.charityhero.live/static/media/logo.798aee74926ff84cf35a2ef12d2ba892.svg"),
			LinkSiteCompany = new Uri("https://www.linkedin.com/company/charitylabfoundation/about/"),
			Projects =
			[
				KeyValuePair.Create("Charity Hero", new ProjectLink(LinkAndroid: "https://play.google.com/store/apps/details?id=com.magnetto.CharityHero&hl=ru&gl=US",
																	LinkiOS: "https://apps.apple.com/us/app/charity-hero/id1612487637",
																	LinkWindow: "")),
				KeyValuePair.Create("Charity Hero Ocean", new ProjectLink(LinkAndroid: "", LinkiOS: "", LinkWindow: "")),
			],
		};

		/// <summary>
		/// Данные о третьем рабочем опыте.
		/// </summary>
		public static WorkExperience ThirdWorkExperience { get; } = new WorkExperience
		{
			NameCompany = "Expert Solution",
			Position = ".NET MAUI Developer",
			Start = new DateTime(2022, 9, 1),
			End = DateTime.MinValue,
			WorkDescription = Resource.SolutionExpertsDescription,
			LinkPhoto = new Uri("https://expertsolution.com.ua/image/catalog/system/expertsolution_logo.png"),
			LinkSiteCompany = new Uri("https://expertsolution.com.ua/"),
			Projects =
			[
				KeyValuePair.Create("ServioPOS", new ProjectLink(LinkAndroid: "https://play.google.com/store/apps/details?id=com.expresssolutions.serviopos",
																LinkiOS: "https://apps.apple.com/ua/app/serviopos/id6444285788?l=uk",
																LinkWindow: "https://apps.microsoft.com/detail/9PM84GX2J8FH?hl=uk-UA&gl=US")),
			],
		};
	}
}