﻿using PortfolioWebsite.Data;

namespace PortfolioWebsite.Services
{
	/// <summary>
	/// Сервис для работы с данными о навыках.
	/// </summary>
	public class SkillService
	{
		/// <summary>
		/// Массив навыков программирования.
		/// </summary>
		public static Skill[] Languages { get; } =
		[
			new Skill
			{
				Name = "C#",
			},
			new Skill
			{
				Name = "XAML",
			},
			new Skill
			{
				Name = "C/C++",
			},
		];

		/// <summary>
		/// Массив навыков работы с базами данных.
		/// </summary>
		public static Skill[] DataBase { get; } =
		[
			new Skill
			{
				Name = "SQLite",
			},
			new Skill
			{
				Name = "SQL Server",
			},
			new Skill
			{
				Name = "MySQL"
			},
			new Skill
			{
				Name = "LiteDB",
			},
			new Skill
			{
				Name = "Realm database (MongoDB)",
			},
			new Skill
			{
				Name = "Firebase (Auth, Firestore Database, Realtime Database, Storage, Hosting)",
			},
			new Skill
			{
				Name = "Azure (Database)",
			},
		];

		/// <summary>
		/// Массив навыков работы с платформами.
		/// </summary>
		public static Skill[] Platforms { get; } =
		[
			new Skill
			{
				Name = "MAUI",
			},
			new Skill
			{
				Name = "Xamarin",
			},
			new Skill
			{
				Name = "Avalonia",
			},
			new Skill
			{
				Name = "WinForms",
			},
			new Skill
			{
				Name = "UWP",
			},
			new Skill
			{
				Name = "WPF",
			},
			new Skill
			{
				Name = "Blazor",
			},
		];

		/// <summary>
		/// Навык работы с системами контроля версий.
		/// </summary>
		public static Skill Git { get; } = new Skill
		{
			Name = "GitLab/GitHub",
		};
	}
}