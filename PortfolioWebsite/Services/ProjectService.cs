﻿using Microsoft.AspNetCore.Components;

using PortfolioWebsite.Data;
using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Services
{
	/// <summary>
	/// Сервис для работы с данными о проектах.
	/// </summary>
	public class ProjectService
	{
		/// <summary>
		/// Данные о проекте Ermolino.
		/// </summary>
		public static Project Ermolino { get; } = new Project
		{
			NameProject = "Ermolino",
			ProjectLink = new Uri("https://gitlab.com/karosveduni/ermolino"),
			Description = new MarkupString(Resource.ErmolinoDescription),
			PhotoLink = new Uri("https://gitlab.com/uploads/-/system/project/avatar/25465451/_66c67016-b69d-422f-841c-a745010c9d07.jpeg?width=128"),
		};

		/// <summary>
		/// Данные о проекте CooverBox.
		/// </summary>
		public static Project CooverBox { get; } = new Project
		{
			NameProject = "CooverBox",
			ProjectLink = new Uri("https://gitlab.com/karosveduni/cooverbox"),
			PhotoLink = new Uri("https://cooverbox.in.ua/local/templates/main/images/logo.svg"),
			Description = new MarkupString(Resource.CooverBoxDescription),
		};

		/// <summary>
		/// Данные о проекте MPI.
		/// </summary>
		public static Project MPI { get; } = new Project
		{
			NameProject = "MPI",
			ProjectLink = new Uri("https://gitlab.com/karosveduni/coursework_mpi"),
			PhotoLink = new Uri("https://gitlab.com/uploads/-/system/project/avatar/31825633/_7a5f48c9-6eb8-44dd-a38c-14e4dc880633.jpeg?width=128"),
			Description = new MarkupString(Resource.CourseworkMPIDescription),
		};

		/// <summary>
		/// Данные о проекте SkyLife.
		/// </summary>
		public static Project SkyLife { get; } = new Project
		{
			NameProject = "SkyLife",
			ProjectLink = new Uri("https://gitlab.com/karosveduni/skylife"),
			PhotoLink = new Uri("https://gitlab.com/uploads/-/system/project/avatar/32657583/logo-835.jpg?width=128"),
			Description = new MarkupString(Resource.SkyLifeDescription)
		};
	}
}