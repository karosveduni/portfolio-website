﻿using PortfolioWebsite.Data;
using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Services
{
	/// <summary>
	/// Сервис для работы с данными об образовании.
	/// </summary>
	public class EducationService
	{
		/// <summary>
		/// Данные о первом образовании.
		/// </summary>
		public static Education FirstEducation { get; } = new Education
		{
			NameUniversity = Resource.NameDNU,
			Speciality = Resource.DNUSpeciality,
			LevelEducation = Enum.LevelEducation.Higher,
			StartDate = 2020,
			EndDate = 2022,
			LinkPhoto = new Uri("https://www.dnu.dp.ua/images/gerb_small.png"),
			LinkSiteUniversity = new Uri("https://www.dnu.dp.ua/")
		};

		/// <summary>
		/// Данные о втором образовании.
		/// </summary>
		public static Education SecondEducation { get; } = new Education
		{
			NameUniversity = Resource.NameCRSE,
			Speciality = Resource.CRSESpeciality,
			LevelEducation = Enum.LevelEducation.Higher,
			StartDate = 2017,
			EndDate = 2020,
			LinkPhoto = new Uri("https://dkrkm.org.ua/sites/all/themes/professional_responsive_theme/logonew1.png"),
			LinkSiteUniversity = new Uri("http://dkrkm.org.ua/")
		};
	}
}