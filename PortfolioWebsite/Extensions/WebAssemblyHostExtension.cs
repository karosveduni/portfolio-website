﻿using System.Globalization;

using Blazored.LocalStorage;

using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

namespace PortfolioWebsite.Extensions
{
	/// <summary>
	/// Класс расширений для WebAssemblyHost.
	/// </summary>
	public static class WebAssemblyHostExtension
	{
		/// <summary>
		/// Метод для установки культуры по умолчанию в WebAssemblyHost.
		/// </summary>
		/// <param name="host">Экземпляр WebAssemblyHost.</param>
		public async static Task SetDefaultCulture(this WebAssemblyHost host)
		{
			ILocalStorageService localStorage = host.Services.GetRequiredService<ILocalStorageService>();

			// Получение строки культуры из локального хранилища.
			string? cultureString = await localStorage.GetItemAsync<string>("culture");

			// Установка культуры на основе строки культуры или значения по умолчанию "en".
			CultureInfo culture = cultureString is not null ? new CultureInfo(cultureString) : new CultureInfo("en");

			// Установка культуры потока по умолчанию.
			CultureInfo.DefaultThreadCurrentCulture = culture;
			CultureInfo.DefaultThreadCurrentUICulture = culture;
		}
	}
}

