﻿using PortfolioWebsite.Enum;
using PortfolioWebsite.Shared.ResourceFiles;

namespace PortfolioWebsite.Extensions
{
	/// <summary>
	/// Класс расширений для перечисления LevelEducation.
	/// </summary>
	public static class LevelEducationExtension
	{
		/// <summary>
		/// Метод для получения перевода уровня образования.
		/// </summary>
		/// <param name="levelEducation">Уровень образования.</param>
		/// <returns>Перевод уровня образования.</returns>
		public static string Translation(this LevelEducation levelEducation)
		{
			return levelEducation switch
			{
				LevelEducation.Secondary => Resource.LevelEducation_Secondary,
				LevelEducation.SecondarySpecial => Resource.LevelEducation_SecondarySpecial,
				LevelEducation.IncompleteHigher => Resource.LevelEducation_IncompleteHigher,
				LevelEducation.Higher => Resource.LevelEducation_Higher,
				_ => nameof(levelEducation),
			};
		}
	}
}