# Портфолио

Добро пожаловать на мой сайт портфолио! Здесь вы найдете информацию о моем образовании, профессиональном опыте, проектах и навыках.

## Образование

- **[Первое образование](https://www.dnu.dp.ua/)**: Відокремлений структурний підрозділ "ФАХОВИЙ КОЛЕДЖ РАКЕТНО-КОСМІЧНОГО МАШИНОБУДУВАННЯ ДНІПРОВСЬКОГО НАЦІОНАЛЬНОГО УНІВЕРСИТЕТУ імені Олеся Гончара" (2017-2020)
  - Специальность: 121 Інженер програмного забеспечення

- **[Второе образование](http://dkrkm.org.ua/)**: Национальный университет имени Олеся Гончара (2020-2022)
  - Специальность: 122 комп'ютерні науки

## Профессиональный опыт

- **[Microinvest](https://microinvest.su/)** (2021-2022)
  - Должность: Xamarin Developer
  - Проекты:
    - [Sendera](https://play.google.com/store/apps/details?id=net.Microinvest.Sendera&hl=ru&gl=US)
    - [Microinvest Control Pro](https://play.google.com/store/apps/details?id=net.controlpro&hl=ru&gl=US)
    - [Хотесса](https://play.google.com/store/apps/details?id=microinvest.Hotessa&hl=ru&gl=US)

- **[CharityLab Foundation](https://www.linkedin.com/company/charitylabfoundation/about/)** (2022-наст. время)
  - Должность: Xamarin Developer
  - Проекты:
    - [Charity Hero](https://play.google.com/store/apps/details?id=com.magnetto.CharityHero&hl=ru&gl=US)
    - [Charity Hero Ocean](ссылка отсутствует)

- **[Expert Solution](https://expertsolution.com.ua/)** (2022-наст. время)
  - Должность: .NET MAUI Developer
  - Проекты:
    - [ServioPOS](https://play.google.com/store/apps/details?id=com.expresssolutions.serviopos)

## Проекты

- **[Ermolino](https://gitlab.com/karosveduni/ermolino)**
  
- **[CooverBox](https://gitlab.com/karosveduni/cooverbox)**
  
- **[MPI](https://gitlab.com/karosveduni/coursework_mpi)**
  
- **[SkyLife](https://gitlab.com/karosveduni/skylife)**

- **[Інші проекти](https://gitlab.com/karosveduni)**
  
## Навыки

### Языки программирования

- C#
- XAML
- C/C++

### Базы данных

- SQLite
- SQL Server
- MySQL
- LiteDB
- Realm (MongoDB)
- Firebase (Auth, Firestore, Realtime Database, Storage, Hosting)
- Azure (Database)

### Платформы

- MAUI
- Xamarin
- Avalonia
- WinForms
- UWP
- WPF
- Blazor

### Системы контроля версий

- GitLab/GitHub

## Контакты

Если у вас есть вопросы или предложения, пожалуйста, свяжитесь со мной через [LinkedIn](https://www.linkedin.com/in/illya-rybak-24442923a/).

# Ссылка на сайт [здесь](https://portfolio-website-illya-rybak.web.app/).